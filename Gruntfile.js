module.exports = function(grunt) {
	grunt.loadNpmTasks("grunt-contrib-concat");
	grunt.loadNpmTasks("grunt-contrib-jshint");
	grunt.loadNpmTasks("grunt-contrib-uglify");
	var dir;
	dir=".";
	grunt.initConfig({	
		dir:dir,
		jshint: {
			options: {
				jshintrc: ".jshintrc"
			},
			files: ["assets/js/*.js"]
		},
		uglify: {
			full:{
				options: {
					sourceMap: false,
					sourceMapURL: '/dist/app.js.map"'
				},
				files: {
					'<%=dir%>/dist/app.js': [
						'<%=dir%>/assets/js/vendor/classie.js',
						'<%=dir%>/assets/js/vendor/scroll.js',
						'<%=dir%>/assets/js/vendor/vanillatop.min.js',
						'<%=dir%>/assets/js/vendor/fullcalendar/main.min.js',
						'<%=dir%>/assets/js/vendor/fullcalendar/i_main.min.js',
						'<%=dir%>/assets/js/vendor/fullcalendar/d_main.min.js',
						'<%=dir%>/assets/js/vendor/fullcalendar/L_main.min.js',
						'<%=dir%>/assets/js/vendor/fullcalendar/g_main.min.js',
						'<%=dir%>/assets/js/vendor/swiper.min.js',
						'<%=dir%>/assets/js/vendor/intro.js',
						'<%=dir%>/assets/js/app.js',
						'<%=dir%>/assets/js/app-intro.js',
						'<%=dir%>/assets/js/app-calendar.js',
						'<%=dir%>/assets/js/app-cookiewarning.js',
						'<%=dir%>/assets/js/app-swiper/app-swiper-centri.js',
						'<%=dir%>/assets/js/app-swiper/app-swiper-editoria.js',
						'<%=dir%>/assets/js/app-swiper/app-swiper-eventi.js',
						'<%=dir%>/assets/js/app-swiper/app-swiper-laboratori.js',
						'<%=dir%>/assets/js/app-swiper/app-swiper-news.js',
						'<%=dir%>/assets/js/app-swiper/app-swiper-progetti.js',
					]
				}				
			},
			base:{
				options: {
					sourceMap: false,
					sourceMapURL: '/dist/app.js.map"'
				},
				files: {
					'<%=dir%>/dist/app-base.js': [
						'<%=dir%>/assets/js/vendor/classie.js',
						'<%=dir%>/assets/js/vendor/scroll.js',
						'<%=dir%>/assets/js/vendor/vanillatop.min.js',
						'<%=dir%>/assets/js/vendor/swiper.min.js',
						'<%=dir%>/assets/js/vendor/intro.js',
						'<%=dir%>/assets/js/app.js',
						'<%=dir%>/assets/js/app-cookiewarning.js',
						'<%=dir%>/assets/js/app-swiper/app-swiper-eventi.js',
						'<%=dir%>/assets/js/app-swiper/app-swiper-news.js',
					]
				}				
			}
		}
	});
	grunt.registerTask("min_full_js", ['uglify:full']);
	grunt.registerTask("min_js", ['uglify:base']);
}