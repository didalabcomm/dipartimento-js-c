# Js dei nuovi siti di dipartimento unifi

  

## /assets/js

  

#### app.js
js con le funzioni base per i menu, per gli indici degli h2, e utility minori

#### app-calendar.js
js per attivare fullcalendar con il plugin di google calendar
 
Importante cambiare l'API key

#### app-cookiewarning.js
js per attivare il messaggio di accettazione cookie

#### app-intro.js
js per attivare intro-js


### /assets/js/app-swiper

js per i vari tipi di carousel della home page

### /assets/js/vendor

js di plugin e classi di terze parti

  

### index.html

usa il file app.js compilato con tutte le funzionalità

### index-dev.html

usa tutti i file js senza nessuna compilazione

  

# Grunt js compiler

  

Crea un file compilato e minificato basato su file indicati nei task (vedi Gruntfiles.js)

  

## Requisiti

  

Node + npm + Grunt

  

[Node+npm](https://www.npmjs.com/get-npm)

[Grunt](https://gruntjs.com)

  

## Installazione

  

cd ~ dipartimento-js-c

  

npm install

  

## Task

  

grunt min_full_js

  

grunt min_js

  

altri task sono configurabili editando Gruntfiles.js

  

## Riferimenti

  

[Grunt](https://gruntjs.com)