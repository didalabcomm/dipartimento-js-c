document.addEventListener('DOMContentLoaded', function () {
    var calendarEl = document.getElementById('month');
    if (calendarEl) {
        var calendar = new FullCalendar.Calendar(calendarEl, {
            plugins: ['interaction', 'dayGrid', 'list', 'googleCalendar'],
            header: {
                left: 'prev',
                center: 'title',
                right: 'next'
            },
            firstDay: 1,
            displayEventTime: false, 
            googleCalendarApiKey: 'AIzaSyDFsIPEp3JRPUtPeiSSMK7nueGvVR-KlfM',
            events: 'ejfpelivol0rvjl82md02i7nes@group.calendar.google.com',
            eventClick: function (arg) {
                window.open(arg.event.url, 'google-calendar-event', 'width=700,height=600');
                arg.jsEvent.preventDefault()
            },
            loading: function (bool) {
                document.getElementById('loading').style.display =
                    bool ? 'block' : 'none';
            },
            dateClick: function (info) {
                dayClicked(info.dateStr);
            }
        });
        calendar.render();
        var calendarEl = document.getElementById('week');
        var calendar1 = new FullCalendar.Calendar(calendarEl, {
            plugins: ['list', 'googleCalendar'],
            header: {
                left: 'today',
                center: '',
                right: 'title'
            },
            views: {
                listDay: { buttonText: 'list day' },
                listWeek: { buttonText: 'list week' }
            },
            defaultView: 'listWeek',
            displayEventTime: true,
            googleCalendarApiKey: 'AIzaSyDFsIPEp3JRPUtPeiSSMK7nueGvVR-KlfM',
            events: 'ejfpelivol0rvjl82md02i7nes@group.calendar.google.com',
            eventRender: function (event, el, view) {
                console.log(event.view);
            },
            eventClick: function (arg) {
                window.open(arg.event.url, 'google-calendar-event', 'width=700,height=600');
                arg.jsEvent.preventDefault() // don't navigate in main tab
            },
            loading: function (bool) {
                document.getElementById('loading').style.display =
                    bool ? 'block' : 'none';
            }
        });
        calendar1.render();
        function dayClicked(date) {
            calendar1.gotoDate(date)
        }
        function sinkToday() {
            calendar.gotoDate(Date.now());
            calendar1.gotoDate(Date.now());
        }
        function enable(el) {
            el.removeAttribute("disabled");
            sinkToday();
        }
        var classname = document.getElementsByClassName("fc-today-button");
        var el = document.querySelector(".fc-left button");
        for (var i = 0; i < el.length; i++) {
            el[i].addEventListener('click', enable, false);
        }
        for (var i = 0; i < classname.length; i++) {
            classname[i].removeAttribute("disabled");
            classname[i].addEventListener('click', sinkToday, false);
        }
    }
});