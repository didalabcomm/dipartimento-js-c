function getCookie(c_name) {
    var i, x, y, ARRcookies = document.cookie.split(";");
    for (i = 0; i < ARRcookies.length; i++) {
        x = ARRcookies[i].substr(0, ARRcookies[i].indexOf("="));
        y = ARRcookies[i].substr(ARRcookies[i].indexOf("=") + 1);
        x = x.replace(/^\s+|\s+$/g, "");
        if (x == c_name) {
            return unescape(y);
        }
    }
}
function displayNotification() {
    warning=document.getElementById("cookiewarning");
    classie.remove(warning, 'hide');
}
function doAccept() {
    setCookie("jsCookieCheck", null, 365);
    warning=document.getElementById("cookiewarning");
    classie.add(warning, 'hide');  
}

function setCookie(c_name, value, exdays) {
    var exdate = new Date();
    exdate.setDate(exdate.getDate() + exdays);
    var c_value = escape(value) + ((exdays == null) ? "" : "; expires=" + exdate.toUTCString() + "; path=/");
    document.cookie = c_name + "=" + c_value;    
}
function checkCookie() {
    var cookieName = "jsCookieCheck";
    var cookieChk = getCookie(cookieName);
    if (cookieChk != null && cookieChk != "") {
        setCookie(cookieName, cookieChk, 365); // un anno.
    }
    else {
        displayNotification();
    }
}
if(document.getElementById("cookiewarning")){
    checkCookie();
}
