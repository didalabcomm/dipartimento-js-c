function startIntro() {
	var iframe = inIframe ();
	if(!iframe){
	el = document.getElementsByTagName('html')[0];
	classie.add(el, 'intro_on');
	if(window.location.host=='dipartimento.test'){
		path= window.location.pathname
	}
	if(window.location.host=='dipartimento.didacommunicationlab.it'){
		path=window.location.pathname
	}
	if(window.location.host=='wwwnt8.unifi.it'){
		path="/";
		if(window.location.pathname=='/p130.html'){
			path='/erbariotropicale';
		}
		if(window.location.pathname=='/vp-17-presentazione.html'){
			path='/dipartimento/presentazione';
		}
	}
	if(window.location.host=='www.dides.unifi.it'){
		path="/";
		if(window.location.pathname=='/p130.html'){
			path='/erbariotropicale';
		}
		if(window.location.pathname=='/vp-17-presentazione.html'){
			path='/dipartimento/presentazione';
		}
	}
	var request = new XMLHttpRequest();
	var url = 'https://admin.dipartimento.didacommunicationlab.it/dipartimento/items/intro?fields=*&filter[url]='+path;
	request.open('GET', url, true);
	request.onload = function () {
		if (this.status >= 200 && this.status < 400) {
			replaced= this.response.replace(/""/g, 'null');
			replaced= replaced.replace(/#go_tematica/g, 'javascript:openPaginaS();');
			replaced= replaced.replace(/#go_pagina/g, 'javascript:openPaginaInterna();');

			var steps = JSON.parse(replaced);
			if(steps.data != ''){
			intro = introJs();
			//qui va gestito 
			intro.onbeforechange(function(targetElement) {
				if(!targetElement.id){
					window.scrollTo(0, 0);
				}
				if(targetElement.getAttribute("id")=="intro_menu_ateneo" ){
					window.scrollTo(0, 0);
				}
				if(targetElement.getAttribute("id")=="intro_contenuto" ){
					window.scrollTo(0, 0);
				}
				if(targetElement.getAttribute("id")=="intro_header_dipartimento" ){
					window.scrollTo(0, 0);
				}
				if(targetElement.getAttribute("id")=="intro_logo" ){
					window.scrollTo(0, 0);
				}
				if(targetElement.getAttribute("id")=="intro_cerca_sito" ){
					window.scrollTo(0, 0);
				}
				if(targetElement.getAttribute("id")=="intro_lingue" ){
					window.scrollTo(0, 0);
				}
				if(targetElement.getAttribute("id")=="intro_btn_menu" ){
					window.scrollTo(0, 0);
					el = document.getElementById('app');
					if(classie.has(el, 'open-right')){
						openSide('right');
					}					
				}else if(targetElement.getAttribute("id")=="intro_submenu" ){
					window.scrollTo(0, 0);
					if(!classie.has(el, 'open-right')){
						openSide('right');
					}
					el=document.querySelectorAll(".main-menu li a")[1];
					openSubMenu(el);
					intro.setOptions('hideNext','true')
				}else if(targetElement.getAttribute("id")=="intro_menu" ){
					window.scrollTo(0, 0);
					el = document.getElementById('app');
					if(!classie.has(el, 'open-right')){
					openSide('right');
					}
				}else if(targetElement.getAttribute("id")=="intro_news"){
					window.scrollTo(0, 0);
					if(classie.has(el, 'open-right')){
						openSide('right');
					}
					classie.add(targetElement, 'intro_show')
				}else if(targetElement.getAttribute("id")=="intro_eventi"){
					classie.add(targetElement, 'intro_show')
				}else if(targetElement.getAttribute("id")=="intro_centri"){
					classie.add(targetElement, 'intro_show')
				}else if(targetElement.getAttribute("id")=="intro_laboratori"){
					classie.add(targetElement, 'intro_show')
				}else if(targetElement.getAttribute("id")=="intro_progetti"){
					classie.add(targetElement, 'intro_show')
				}else if(targetElement.getAttribute("id")=="intro_agenda"){
					classie.add(targetElement, 'intro_show')
				}else if(targetElement.getAttribute("id")=="intro_editoria"){
					classie.add(targetElement, 'intro_show')
				}else if(targetElement.getAttribute("id")=="intro_video"){
					classie.add(targetElement, 'intro_show')
				}else if(targetElement.getAttribute("id")=="intro_avvisi"){
					classie.add(targetElement, 'intro_show')
				}else if(targetElement.getAttribute("id")=="intro_sba"){
					classie.add(targetElement, 'intro_show')
				}else{
					el = document.getElementById('app');
					if(classie.has(el, 'open-right')){	
						openSide('right');
					}	
				}				
			  }); 
			  intro.onafterchange(function(targetElement) {
				if(targetElement.getAttribute("id")=="intro_contenuto" ){
					setTimeout(function () {
						window.scrollTo(0, 0);
					}, 500);
					
				}
			});			
			intro.setOptions({ steps: steps.data });
			intro.onexit(function() {
				el = document.getElementsByTagName('html')[0];
				classie.remove(el, 'intro_on');
			  });
			intro.start();
			}
		} else {
			console.log('il server da errore');
		}
	};
	request.onerror = function () {
		console.log('errore di connessione impossibile effettuare la richiesta');
	};
	request.send();
}
}


function inIframe () {
    try {
        return window.self !== window.top;
    } catch (e) {
        return true;
    }
}

function openPaginaInterna(){
	if(window.location.host=='dipartimento.test'){
		window.location.href='http://dipartimento.test/dipartimento/presentazione'
	}
	if(window.location.host=='dipartimento.didacommunicationlab.it'){
		window.location.href='https://dipartimento.didacommunicationlab.it/dipartimento/presentazione'
	}
	if(window.location.host=='wwwnt8.unifi.it'){
		window.location.href='https://wwwnt8.unifi.it/vp-17-presentazione.html'
	}
	if(window.location.host=='www.dides.unifi.it'){
		window.location.href='https://www.dides.unifi.it/vp-17-presentazione.html'
	}
}

function openPaginaS(){
	if(window.location.host=='dipartimento.test'){
		window.location.href='http://dipartimento.test/erbariotropicale'
	}
	if(window.location.host=='dipartimento.didacommunicationlab.it'){
		window.location.href='https://dipartimento.didacommunicationlab.it/erbariotropicale'
	}
	if(window.location.host=='wwwnt8.unifi.it'){
		window.location.href='https://wwwnt8.unifi.it/p130.html'
	}
	if(window.location.host=='www.dides.unifi.it'){
		window.location.href='https://www.dides.unifi.it/p130.html'
	}
}