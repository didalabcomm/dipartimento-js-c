var swiper_centri = new Swiper('#centri', {
	slidesPerView: 1,
	spaceBetween: 10,
	preloadImages: true,
	loop: false,
	pagination: {
		el: '#centri .swiper-pagination',
		clickable: true,
	},
	navigation: {
		nextEl: '.swiper-button-next',
		prevEl: '.swiper-button-prev',
	},
	breakpoints: {
		640: {
			slidesPerView: 2,
			spaceBetween: 8,
		},
		768: {
			slidesPerView: 3,
			spaceBetween: 12,
		},
		1024: {
			slidesPerView: 4,
			spaceBetween: 12,
		},

	},
	on: {
		init: function () {
			setTimeout(function () {
				console.log(swiper_centri.el);
				if (swiper_centri.isBeginning && swiper_centri.isEnd) {
					arrowDisplay("#centri .swiper-button-next", false);
					arrowDisplay("#centri .swiper-button-prev", false);
				} else if (swiper_centri.isBeginning) {
					arrowDisplay("#centri .swiper-button-prev", false);
				}
			}, 500);
		},
		slideChange: function () {
			if (swiper_centri.isBeginning) {
				arrowDisplay("#centri .swiper-button-prev", false);
				arrowDisplay("#centri .swiper-button-next", true);
			}
			else if (swiper_centri.isEnd) {
				arrowDisplay("#centri .swiper-button-next", false);
				arrowDisplay("#centri .swiper-button-prev", true);
			}
			else {
				arrowDisplay("#centri .swiper-button-next", true);
				arrowDisplay("#centri .swiper-button-prev", true);
			}
		},
	}
});