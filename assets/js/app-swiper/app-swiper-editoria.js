var swiper = new Swiper('#editoria', {
	slidesPerView: 2,
	spaceBetween: 10,
	loop: true,
	preloadImages: true,
	pagination: {
		el: '#editoria .swiper-pagination',
		clickable: true,
	},
	navigation: {
		nextEl: '.swiper-button-next',
		prevEl: '.swiper-button-prev',
	},
	breakpoints: {
		640: {
			slidesPerView: 3,
			spaceBetween: 8,
		},
		768: {
			slidesPerView: 4,
			spaceBetween: 12,
		},
		1024: {
			slidesPerView: 5,
			spaceBetween: 24,
		},
		1440: {
			slidesPerView: 7,
			spaceBetween: 36,
		},
	}
});