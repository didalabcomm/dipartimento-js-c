var swiper_eventi = new Swiper('#eventi', {
	slidesPerView: 1,
	spaceBetween: 10,
	loop: false,
	preloadImages: true,
	navigation: {
		nextEl: '.swiper-button-next',
		prevEl: '.swiper-button-prev',
	},
	pagination: {
		el: '#eventi .swiper-pagination',
		clickable: true,
	},
	breakpoints: {
		640: {
			slidesPerView: 2,
			spaceBetween: 8,
		},
		768: {
			slidesPerView: 2,
			spaceBetween: 12,
		},
		1024: {
			slidesPerView: 3,
			spaceBetween: 12,
		},
	},
	on: {
		init: function () {
			setTimeout(function () {
				if (swiper_eventi.isBeginning && swiper_eventi.isEnd) {
					arrowDisplay("#eventi .swiper-button-next", false);
					arrowDisplay("#eventi .swiper-button-prev", false);
				} else if (swiper_eventi.isBeginning) {
					arrowDisplay("#eventi .swiper-button-prev", false);
				}
			}, 50);
		},
		slideChange: function () {
			if (swiper_eventi.isBeginning) {
				arrowDisplay("#eventi .swiper-button-prev", false);
				arrowDisplay("#eventi .swiper-button-next", true);
			}
			else if (swiper_eventi.isEnd) {
				arrowDisplay("#eventi .swiper-button-next", false);
				arrowDisplay("#eventi .swiper-button-prev", true);
			}
			else {
				arrowDisplay("#eventi .swiper-button-next", true);
				arrowDisplay("#eventi .swiper-button-prev", true);
			}
		},
	}
});