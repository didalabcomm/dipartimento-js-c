var swiper_laboratori = new Swiper('#laboratori', {
	slidesPerColumn: 2,
	slidesPerView: 1,
	spaceBetween: 12,
	preloadImages: true,
	loop: false,
	navigation: {
		nextEl: '.swiper-button-next',
		prevEl: '.swiper-button-prev',
	},
	pagination: {
		el: '#laboratori .swiper-pagination',
		clickable: true,
	},
	breakpoints: {
		640: {
			slidesPerView: 1,
			slidesPerColumn: 2,
			spaceBetween: 8,
		},
		768: {
			slidesPerView: 2,
			slidesPerColumn: 2,
			spaceBetween: 12,
		},
		1024: {
			slidesPerView: 3,
			slidesPerColumn: 2,
			spaceBetween: 12,
		},
		1440: {
			slidesPerView: 3,
			slidesPerColumn: 2,
			spaceBetween: 12,
		},
	},
	on: {
		init: function () {
			setTimeout(function () {
				if (swiper_laboratori.isBeginning && swiper_laboratori.isEnd) {
					arrowDisplay("#laboratori .swiper-button-next", false);
					arrowDisplay("#laboratori .swiper-button-prev", false);
				} else if (swiper_laboratori.isBeginning) {
					arrowDisplay("#laboratori .swiper-button-prev", false);
				}
			}, 50);
		},
		slideChange: function () {
			if (swiper_laboratori.isBeginning) {
				arrowDisplay("#laboratori .swiper-button-prev", false);
				arrowDisplay("#centri .swiper-button-next", true);
			}
			else if (swiper_laboratori.isEnd) {
				arrowDisplay("#laboratori .swiper-button-next", false);
				arrowDisplay("#laboratori .swiper-button-prev", true);
			}
			else {
				arrowDisplay("#laboratori .swiper-button-next", true);
				arrowDisplay("#laboratori .swiper-button-prev", true);
			}
		},
	}
});