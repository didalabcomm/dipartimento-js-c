var swiper_news = new Swiper('#news', {
	spaceBetween: 10,
	loop: false,
	preloadImages: true,
	pagination: {
		el: '#news .swiper-pagination',
	},
	breakpoints: {
		640: {
			slidesPerView: 2,
			spaceBetween: 8,

		},
		768: {
			slidesPerView: 3,
			spaceBetween: 12,
		},
		1024: {
			slidesPerView: 4,
			spaceBetween: 12,
		},
	},
	on: {
		init: function () {
			setTimeout(function () {
				if (swiper_news.isBeginning && swiper_news.isEnd) {
					arrowDisplay("#news .swiper-button-next", false);
					arrowDisplay("#news .swiper-button-prev", false);
				} else if (swiper_news.isBeginning) {
					arrowDisplay("#news .swiper-button-prev", false);
				}
			}, 3000);
		},
		slideChange: function () {
			if (swiper_news.isBeginning) {
				arrowDisplay("#news .swiper-button-prev", false);
				arrowDisplay("#news .swiper-button-next", true);
			}
			else if (swiper_news.isEnd) {
				arrowDisplay("#news .swiper-button-next", false);
				arrowDisplay("#news .swiper-button-prev", true);
			}
			else {
				arrowDisplay("#news .swiper-button-next", true);
				arrowDisplay("#news .swiper-button-prev", true);
			}
		},
	}
});








