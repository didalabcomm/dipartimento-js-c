var swiper_progetti = new Swiper('#progetti', {
	slidesPerView: 1,
	spaceBetween: 10,
	loop: false,
	preloadImages: true,
	pagination: {
		el: '#progetti .swiper-pagination',
		clickable: true,
	},
	navigation: {
		nextEl: '.swiper-button-next',
		prevEl: '.swiper-button-prev',
	},
	breakpoints: {
		640: {
			slidesPerView: 2,
			spaceBetween: 8,
		},
		768: {
			slidesPerView: 3,
			spaceBetween: 12,
		},
		1024: {
			slidesPerView: 4,
			spaceBetween: 12,
		},
	},
	on: {
		init: function () {
			setTimeout(function () {
				if (swiper_progetti.isBeginning && swiper_progetti.isEnd) {
					arrowDisplay("#progetti .swiper-button-next", false);
					arrowDisplay("#progetti .swiper-button-prev", false);
				} else if (swiper_progetti.isBeginning) {
					arrowDisplay("#progetti .swiper-button-prev", false);
				}
			}, 50);
		},
		slideChange: function () {
			if (swiper_progetti.isBeginning) {
				arrowDisplay("#progetti .swiper-button-prev", false);
				arrowDisplay("#progetti .swiper-button-next", true);
			}
			else if (swiper_progetti.isEnd) {
				arrowDisplay("#progetti .swiper-button-next", false);
				arrowDisplay("#progetti .swiper-button-prev", true);
			}
			else {
				arrowDisplay("#progetti .swiper-button-next", true);
				arrowDisplay("#progetti .swiper-button-prev", true);
			}
		},
	}
});